local robbie = require("robot")
local component = require("component")
local sides = require("sides") -- this might not be needed? maybe required for inventory controller
local inventory = component.inventory_controller
local event = require("event")

-- Successfully defines an item to build
-- assigns that item to "target"
-- parses target to get a requirements table. key is id, value is # required
-- (also has a funtion defined to allow easily printing a table.. it's probably an overkill function)
-- takes two parameters: first is which item to build, second is an optional quantity (must be a number). 
-- there is no default item, the default quantity is 1

local myInv = {} -- used for robots inventory
local thing = default -- name of item from command line
local quantity = 1 -- quanity desired from command line
local required = {} -- how much of what is needed
local need = {} -- of the required, how much is missing from the inventory

args ={...}
if args[1] then
  thing = args[1] -- the thing that is being asked for
end
if tonumber(args[2]) then
  quantity = tonumber(args[2]) -- the number of things being asked for
end

-- start defining function:
-- for printing tables (kinda overkill for our need)
function tprint (tbl, indent)
    if not indent then indent = 0 end
    for k, v in pairs(tbl) do
        formatting = string.rep("  ", indent) .. k .. ": "
        if type(v) == "table" then
            print(formatting)
            tprint(v, indent+1)
        elseif type(v) == 'boolean' then
            print(formatting .. tostring(v))      
        else
            print(formatting .. v)
        end
    end
end

-- this will be our standard definition format.
-- table(array) of tables of tables. 
-- first index is vertical layer (Y level in minecraft)
-- second index is back to front. it's easier to build starting with the back row
-- third is either left to right or right to left. we'll have to decide. (this far items can be mirrored, so it won't matter yet.)
-- -- I think i prefer left to right, like reading in the languages I know.

-- for a given itemName, returns a table with the 'recipe'
-- Not difficult to add more, just follow the pattern

-- table of definitions of things that may be built.
local definition = {}
definition.CompactMachineWall = {
[1] = -- bottom layer
    {
        [3]= -- front row
            {"minecraft:iron_block"}
    },
[2] = -- middle layer
    {
        [3]= -- front row
            {"minecraft:redstone"}
    }
}
-- [3] would be the 3rd layer
definition.CompactMachineWall.catalyst = "minecraft:redstone"
definition.CompactMachineWall.wait = 8
definition.CompactMachineWall.name = "CompactMachineWall"
definition.CompactMachineWall.ID = "compactmachines3:wallbreakable"
definition.CompactMachineWall.Qty = 16

definition.EnderPearl = {
[1] = -- bottom layer
    {
        [1] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"}, -- back row
        [2] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"}, -- middle row
        [3] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"} -- front row
    },
    [2] = -- middle layer
    {
        [1] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"},
        [2] = {"minecraft:obsidian","minecraft:redstone_block","minecraft:obsidian"},
        [3] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"}
    },
    [3] = -- top layer
    {
        [1] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"},
        [2] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"},
        [3] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"}
    }
}
definition.EnderPearl.catalyst = "minecraft:redstone"
definition.EnderPearl.wait = 10
definition.EnderPearl.name = "EnderPearl"
definition.EnderPearl.ID = "minecraft:ender_pearl"
definition.EnderPearl.Qty = 1

definition.Tunnel = {
    [1] = -- bottom layer
        {
            [1] = {"minecraft:redstone","minecraft:redstone","minecraft:redstone"}, -- back row
            [2] = {"minecraft:redstone","compactmachines3:wallbreakable","minecraft:redstone"}, -- middle row
            [3] = {"minecraft:redstone","minecraft:redstone","minecraft:redstone"} -- front row
        },
    [2] = -- middle layer
        {
            [2] = {[2] = "minecraft:hopper"} -- center block
        }
    }
definition.Tunnel.catalyst = "minecraft:redstone"
definition.Tunnel.wait = 10
definition.Tunnel.name = "Tunnel"
definition.Tunnel.ID = "compactmachines3.tunneltool"
definition.Tunnel.Qty = 2

definition.SmallCompactMachine = {
    [1] = -- bottom layer
        {
            [1] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"}, -- back row
            [2] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"}, -- middle row
            [3] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"} -- front row
        },
        [2] = -- middle layer
        {
            [1] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"},
            [2] = {"compactmachines3:wallbreakable","minecraft:iron_block","compactmachines3:wallbreakable"},
            [3] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"}
        },
        [3] = -- top layer
        {
            [1] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"},
            [2] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"},
            [3] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"}
        }
    }
definition.SmallCompactMachine.catalyst = "minecraft:ender_pearl"
definition.SmallCompactMachine.wait = 20
definition.SmallCompactMachine.name = "SmallCompactMachine"
definition.SmallCompactMachine.ID = "compactmachines3:machine"
definition.SmallCompactMachine.Qty = 1

definition.NormalCompactMachine = {
    [1] = -- bottom layer
        {
            [1] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"}, -- back row
            [2] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"}, -- middle row
            [3] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"} -- front row
        },
        [2] = -- middle layer
        {
            [1] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"},
            [2] = {"compactmachines3:wallbreakable","minecraft:gold_block","compactmachines3:wallbreakable"},
            [3] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"}
        },
        [3] = -- top layer
        {
            [1] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"},
            [2] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"},
            [3] = {"compactmachines3:wallbreakable","compactmachines3:wallbreakable","compactmachines3:wallbreakable"}
        }
    }
definition.NormalCompactMachine.catalyst = "minecraft:ender_pearl"
definition.NormalCompactMachine.wait = 25
definition.NormalCompactMachine.name = "NormalCompactMachine"
definition.NormalCompactMachine.ID = "compactmachines3:machine"
definition.NormalCompactMachine.Qty = 1

-- returns a table with the robot's current inventory
-- currently only returns one slot for any given item
-- when moving to 5x5 grids will need to do something (like refresh inventory if it runs out))
function getCurrentInventory() 
    local inv = {}
    for i=1, 16, 1 do
        local item = {}
        item = inventory.getStackInInternalSlot(i)
        if item then 
            if inv[item.name] then   
                robbie.select(inv[item.name].slot)
                if robbie.transferTo(i) then item = inventory.getStackInInternalSlot(i) end
            end
            inv[item.name] = item
            inv[item.name].slot = i
        end
    end
    return inv
end
    
-- returns a table with a key of requirements names and quantity for a given recipe
function getItemRequirements(target)
    local requirements = {}
    if target then 
        for y=1, 3, 1 do
            if target[y] then
                for x=1, 3, 1 do
                    if target[y][x] then 
                        for z=1, 3, 1 do
                            if target[y][x][z] then   
    --                          print(y,x,z,target[y][x][z])
                                if requirements[target[y][x][z]] then
                                    requirements[target[y][x][z]] = requirements[target[y][x][z]] + 1
                                else  requirements[target[y][x][z]] = 1 
                                end
                            end
                        end
                    end
                end
            end
        end
        if requirements[target.catalyst] then
    --      print("Catalyst",target.catalyst)
            requirements[target.catalyst] = requirements[target.catalyst] + 1
        else requirements[target.catalyst] = 1 
        end
    else 
        requirements = nil
    end
    return requirements
end

-- compare inventory to required, return a table of what's missing
function checkForRequirements(inv,requirements)
    local reqcheck = true
    local missing = {}
    for k, v in pairs(requirements) do
      if inv[k] == nil then 
        reqcheck = false
        missing[k] = v
      elseif inv[k].size < v then
            reqcheck = false
            missing[k] = v - inv[k].size
      end
    end
    if reqcheck then
        return nil
    else return missing
    end
end

function getFromChest(needed)
    local chest = {}
    robbie.turnLeft()
    -- check chest
    chest.size = inventory.getInventorySize(sides.front)
    for x = 1, chest.size, 1 do
        chest[x] = inventory.getStackInSlot(sides.front,x)
    end
    chest.size = nil
    -- get applicable items
    for k, v in pairs(chest) do
        if needed[v.name] then 
             if inventory.suckFromSlot(sides.front, k, needed[v.name]) then
                needed[v.name] = needed[v.name] - v.size
                if needed[v.name] <= 0 then needed[v.name] = nil
                end
             end
        end    
    end
    robbie.turnRight()
    return needed
end

function lever()
    robbie.up()
    robbie.turnLeft()
    robbie.forward()
    robbie.turnLeft()
    robbie.use()
    robbie.turnRight()
    robbie.back()
    robbie.turnRight()
    robbie.down()
end

-- actual act of building the thing
function build(target)
    -- assume the robot starts one block away from the grid, on the left side, bottom layer.
    -- if you imaging a 3x3 grid, with the back row being 3, robot is at row -1 (-1, 0, 1, 2, 3), layer one, block 1

    print("I'm going to start building now")
    robbie.forward()
    for y= 1, 3, 1 do
        if target[y] then 
            robbie.forward()
            robbie.forward()
            for x=1, 3, 1 do
                if target[y][x] then
                    for z=1, 3, 1 do
                        if target[y][x][z] then
                            robbie.select(myInv[target[y][x][z]].slot)
                            robbie.place()
                        end
                        if z < 3 then
                            robbie.turnRight()
                            robbie.forward()
                            robbie.turnLeft()
                        end
                    end
                    robbie.turnLeft()
                    robbie.forward()
                    robbie.forward()
                    robbie.turnRight()
                end
                if x < 3 then
                    robbie.back()
                elseif y < 3 then
                    robbie.up()
                end
            end
        end
    end
    robbie.down()
    robbie.down()
    robbie.back()
    
    -- print("now we just throw the catalyst...")
    robbie.select(myInv[target.catalyst].slot)
    robbie.drop(1)
    print("Waiting... (press any key to interrupt)")
    -- os.sleep(target.wait)
    check = event.pull(target.wait,"key_down") -- waits a few seconds for a key to be pressed. if no key is pressed, it will continue the loop.
    if check then -- if a key is pressed, this won't be nil, and the loop is exited
        print("Key pressed -- exiting")
        return 0 -- exit to stop
    end 

    lever()
  
    -- print("OK, now done")
    -- at this point, we should be back at our starting position, ready to go again
    return 1 -- assumes build was good
end
-- Now we have our functions, and we can begin:

target = definition[thing]

if not target then 
    print("Doesn't appear to be a valid item to build. Valid values are:")
    print("","CompactMachineWall")
    print("","EnderPearl")
    print("","Tunnel")
    print("","SmallCompactMachine")
    print("","NormalCompactMachine")
    goto stop
end

function process()
    required = getItemRequirements(target)
    -- print("Total Requirements:")
    -- tprint(required,1)

    myInv = getCurrentInventory()

    -- print("checkForRequirements")
    need = checkForRequirements(myInv,required)
    -- tprint(need)

    -- print("getfromChest")
    if need then 
        need = getFromChest(need)
    end

    myInv = getCurrentInventory()
--    print("Current Robot Inventory:")
--    for k, v in pairs(myInv) do
--        print(k, "Qty:" .. string.format("%u", v.size), "Slot#:" .. v.slot)
--    end

    need = checkForRequirements(myInv,required)

    if need then
        print("Items are missing:")
        tprint(need)
        print("I cannot find the needed materials, so please put the missing items in my chest and try again.")
        return 0
    else print("I have what I need to build the next " .. target.name)
    end

    return build(target)

end

print("Asked to create " .. quantity .. " of " ..  target.name)
if quantity > 1 then
    print("I'll try to build more than one")
end 
for i=1, quantity, 1 do
    if quantity > 1 then print("starting #" .. i) end
    if process() == 0 then 
        if quantity > 1 then print("Unable to create #" .. i) end
        goto stop 
    else print("Completed #" .. i)
    end
end
print("I think I finished everything")

-- I, I-i, beleive in love.
::stop::
local robbie = require("robot")
local component = require("component")
local sides = require("sides") -- this might not be needed? maybe required for inventory controller
local inventory = component.inventory_controller
local event = require("event")

-- This is a specialized builder. The robot will build one thing continuously, 
-- if it has the requirements and the dump chest has at least one empty slot
-- only contains one defintion for which item to build. doesn't hurt to keep more definitions, but no point
-- ****ROBOT POSITIONING IS DIFFERENT. SUPPLY CHEST IS ON THE RIGHT, DUMP CHEST ON THE LEFT****

local myInv = {} -- used for robots inventory
local thing = "EnderPearl" -- name of item from command line
local required = {} -- how much of what is needed
local need = {} -- of hte required, how much is missing from the inventory

-- start defining function:
-- for printing tables (kinda overkill for our need)
function tprint (tbl, indent)
    if not indent then indent = 0 end
    for k, v in pairs(tbl) do
        formatting = string.rep("  ", indent) .. k .. ": "
        if type(v) == "table" then
            print(formatting)
            tprint(v, indent+1)
        elseif type(v) == 'boolean' then
            print(formatting .. tostring(v))      
        else
            print(formatting .. v)
        end
    end
end

-- this uses our standard definition format (see GeneralizedBuilder.lua)
local definition = {}
definition.EnderPearl = {
[1] = -- bottom layer
    {
        [1] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"}, -- back row
        [2] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"}, -- middle row
        [3] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"} -- front row
    },
    [2] = -- middle layer
    {
        [1] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"},
        [2] = {"minecraft:obsidian","minecraft:redstone_block","minecraft:obsidian"},
        [3] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"}
    },
    [3] = -- top layer
    {
        [1] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"},
        [2] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"},
        [3] = {"minecraft:obsidian","minecraft:obsidian","minecraft:obsidian"}
    }
}
definition.EnderPearl.catalyst = "minecraft:redstone"
definition.EnderPearl.wait = 10
definition.EnderPearl.name = "EnderPearl"
definition.EnderPearl.ID = "minecraft:ender_pearl"
definition.EnderPearl.Qty = 1

-- returns a table with the robot's current inventory
-- currently only returns one slot for any given item
-- when moving to 5x5 grids will need to do something (like refresh inventory if it runs out))
function getCurrentInventory() 
    local inv = {}
    for i=1, 16, 1 do
        local item = {}
        item = inventory.getStackInInternalSlot(i)
        if item then 
            if inv[item.name] then   
                robbie.select(inv[item.name].slot)
                if robbie.transferTo(i) then item = inventory.getStackInInternalSlot(i) end
            end
            inv[item.name] = item
            inv[item.name].slot = i
        end
    end
    return inv
end
    
-- returns a table with a key of requirements names and quantity for a given recipe
function getItemRequirements(target)
    local requirements = {}
    if target then 
        for y=1, 3, 1 do
            if target[y] then
                for x=1, 3, 1 do
                    if target[y][x] then 
                        for z=1, 3, 1 do
                            if target[y][x][z] then   
    --                          print(y,x,z,target[y][x][z])
                                if requirements[target[y][x][z]] then
                                    requirements[target[y][x][z]] = requirements[target[y][x][z]] + 1
                                else  requirements[target[y][x][z]] = 1 
                                end
                            end
                        end
                    end
                end
            end
        end
        if requirements[target.catalyst] then
    --      print("Catalyst",target.catalyst)
            requirements[target.catalyst] = requirements[target.catalyst] + 1
        else requirements[target.catalyst] = 1 
        end
    else 
        requirements = nil
    end
    return requirements
end

-- compare inventory to required, return a table of what's missing
function checkForRequirements(inv,requirements)
    local reqcheck = true
    local missing = {}
    for k, v in pairs(requirements) do
      if inv[k] == nil then 
        reqcheck = false
        missing[k] = v
      elseif inv[k].size < v then
            reqcheck = false
            missing[k] = v - inv[k].size
      end
    end
    if reqcheck then
        return nil
    else return missing
    end
end

function getFromChest(needed) -- it's the opposite direction from other builders
    local chest = {}
    robbie.turnRight()
    -- check chest
    chest.size = inventory.getInventorySize(sides.front)
    for x = 1, chest.size, 1 do
        chest[x] = inventory.getStackInSlot(sides.front,x)
    end
    chest.size = nil
    -- get applicable items
    for k, v in pairs(chest) do
        if needed[v.name] then 
             if inventory.suckFromSlot(sides.front, k, needed[v.name]) then
                needed[v.name] = needed[v.name] - v.size
                if needed[v.name] <= 0 then needed[v.name] = nil
                end
             end
        end    
    end
    robbie.turnLeft()
    return needed
end

-- looks at dump chest to see if any slots are empty
function verifySpace()
    print("verifying space")
    robbie.turnLeft()
    local chest = {}
    chest.size = inventory.getInventorySize(sides.front)
    for x = 1, chest.size, 1 do
        if not inventory.getStackInSlot(sides.front,x) then
            robbie.turnRight()
            return true
        end
    end
    robbie.turnRight()
    return false -- no slot was empty
end

function lever()
    robbie.up()
    robbie.turnLeft()
    robbie.forward()
    robbie.turnLeft()
    robbie.use()
    robbie.turnRight()
    robbie.back()
    robbie.turnRight()
    robbie.down()
end

-- actual act of building the thing
function build(target)
    -- assume the robot starts one block away from the grid, on the left side, bottom layer.
    -- if you imaging a 3x3 grid, with the back row being 3, robot is at row -1 (-1, 0, 1, 2, 3), layer one, block 1

    print("I'm going to start building now")
    robbie.forward()
    for y= 1, 3, 1 do
        if target[y] then 
            robbie.forward()
            robbie.forward()
            for x=1, 3, 1 do
                if target[y][x] then
                    for z=1, 3, 1 do
                        if target[y][x][z] then
                            robbie.select(myInv[target[y][x][z]].slot)
                            robbie.place()
                        end
                        if z < 3 then
                            robbie.turnRight()
                            robbie.forward()
                            robbie.turnLeft()
                        end
                    end
                    robbie.turnLeft()
                    robbie.forward()
                    robbie.forward()
                    robbie.turnRight()
                end
                if x < 3 then
                    robbie.back()
                elseif y < 3 then
                    robbie.up()
                end
            end
        end
    end
    robbie.down()
    robbie.down()
    robbie.back()
    
    -- print("now we just throw the catalyst...")
    robbie.select(myInv[target.catalyst].slot)
    robbie.drop(1)
    print("Waiting... (press any key to interrupt)")
    check = event.pull(target.wait,"key_down") -- waits a few seconds for a key to be pressed. if no key is pressed, it will continue the loop.
    if check then -- if a key is pressed, this won't be nil, and the loop is exited
        print("Key pressed -- exiting")
        return 0 -- exit to stop
    end 

    lever()
    return 1 -- assumes build was good if we made it this far
end

function process()
    local OK = true

    required = getItemRequirements(target)
    -- print("Total Requirements:")
    -- tprint(required,1)

    myInv = getCurrentInventory()

    -- print("checkForRequirements")
    need = checkForRequirements(myInv,required)
    -- tprint(need)

    -- print("getfromChest")
    if need then 
        need = getFromChest(need)
    end

    myInv = getCurrentInventory()
--    print("Current Robot Inventory:")
--    for k, v in pairs(myInv) do
--        print(k, "Qty:" .. string.format("%u", v.size), "Slot#:" .. v.slot)
--    end

    need = checkForRequirements(myInv,required)
    print(need)
    OK = verifySpace()
    print(OK)
    
    if OK and not need then
        return build(target)
    end
    return false
    
end

-- Now we have our functions, and we can begin:

target = definition[thing]
if not target then 
    print("Doesn't appear to be a valid item to build. Valid values are:")
    print("","EnderPearl")
    goto stop
end

print("Starting to build " ..  target.name .. " until a key is pressed.")
while true do
    local waitTime = 2
    if process() == 0 then 
        waitTime = 15
    end
    check = event.pull(waitTime,"key_down") -- waits a few seconds for a key to be pressed. if no key is pressed, it will continue the loop.
    if check then -- if a key is pressed, this won't be nil, and the loop is exited
        print("Key pressed -- exiting")
        goto stop -- exit to stop
    end  
end

-- I, I-i, beleive in love.
::stop::
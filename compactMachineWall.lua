local robbie = require("robot")
local component = require("component")
local sides = require("sides")
local inv = {}
local inventory = component.inventory_controller
local required = {["minecraft:iron_block"] = 1,["minecraft:redstone"] = 2}


for k, v in pairs(required) do
    print("Required: " .. k, "Needed: " .. v)
end

-- long term plan is to be able to pass in a definition of how to build something,
-- and let the robot collect and build it on its own
local function CompactMachineWall ()

    local successCheck -- anything goes wrong, we can set it to false
    -- inventory check
    -- This should have calls to something like "getMaterial" to gather
    -- the required materials, and check as they are collected
    -- gather enough to complete
    -- i like the idea of using "slot.Catalyst" to store the catalyst
    -- or just pointing to the inventory slot with the catalyst

    -- thius inventory check only tracks the highest numbered slot for an item. 
    -- For example if you need 2 redstone, and you have one in slot 2 and one in slot 3,
    -- Only the redstone in slot 3 will be counted and there is not enough

    -- quick fix is to transfer items from a previously found slot to this one, but
    -- that has still has a single stack limit (problematic with 5x5 builds)
    for i=1, 16, 1 do
        local item = {}
        item = inventory.getStackInInternalSlot(i)
        if item then 
            if inv[item.name] then   
                print(inv[item.name].label, inv[item.name].slot)
                robbie.select(inv[item.name].slot)
                if robbie.transferTo(i) then item = inventory.getStackInInternalSlot(i) end
            end
            inv[item.name] = item
            inv[item.name].slot = i
        end
    end

    print("Current Robot Inventory:")
    for k, v in pairs(inv) do
        print(k, "Qty:" .. string.format("%u", v.size), "Slot#:" .. v.slot)
    end

    -- check requirements for Compact Machine Walls
    local reqcheck = true
    for k, v in pairs(required) do
      print("ReqCheck:", k, v)
      if inv[k] == nil then 
        reqcheck = false
        print("Robot has no " .. k)
      elseif inv[k].size < v then
            reqcheck = false
            print("Robot has " .. inv[k].size ..  " but needs " .. v ..  " of " .. k)
      else print("Robot has enough " .. k)
      end
    end
    if reqcheck == false then return 0 end

    robbie.forward()
    robbie.select(inv["minecraft:iron_block"].slot)
    successCheck = robbie.place()
    if successCheck == false then error(successCheck) end
    successCheck = robbie.up()
    if successCheck == false then error(successCheck) end
    robbie.select(inv["minecraft:redstone"].slot)
    successCheck = robbie.place()
    if successCheck == false then error(successCheck) end
    robbie.select(inv["minecraft:redstone"].slot)
    -- need to back up away from work grid before dropping the catalyst
    robbie.back()
    robbie.drop(1)
    -- return home
    robbie.down()

    -- if we add repetition, will need a sleep time before returning so it doesn't interfere with the ongoing miniaturization
    return "normal exit"
end
 
print(CompactMachineWall())
local robbie = require("robot")
local component = require("component")
local sides = require("sides") -- this might not be needed? maybe required for inventory controller
local inventory = component.inventory_controller

local chest = {}
local myInv = {}

function getCurrentInventory() 
    local inv = {}
    for i=1, 16, 1 do
        local item = {}
        item = inventory.getStackInInternalSlot(i)
        if item then 
            if inv[item.name] then   
                print(inv[item.name].label, inv[item.name].slot)
                robbie.select(inv[item.name].slot)
                if robbie.transferTo(i) then item = inventory.getStackInInternalSlot(i) end
            end
            inv[item.name] = item
            inv[item.name].slot = i
        end
    end
    return inv
end

need = {["minecraft:iron_block"] = 1,["minecraft:redstone"] = 2}

myInv = getCurrentInventory()
print("Current Robot Inventory:")
for k, v in pairs(myInv) do
    print(k, "Qty:" .. string.format("%u", v.size), "Slot#:" .. v.slot)
end

print("what's needed")
for k, v in pairs(need) do
    print("item: " .. k, "Qty: " .. v)
  end
  
print("")

robbie.turnLeft()
chest.size = inventory.getInventorySize(sides.front)

for x = 1, chest.size, 1 do
chest[x] = inventory.getStackInSlot(sides.front,x)
end

chest.size = nil
print("Current Chest Inventory:")
for k, v in pairs(chest) do
  print("slot: " .. k, v.size, v.name)
end


for k, v in pairs(chest) do
    if need[v.name] then 
         if inventory.suckFromSlot(sides.front, k, need[v.name]) then
            need[v.name] = need[v.name] - v.size
            print("Took some ".. v.name)
            if need[v.name] <= 0 then need[v.name] = nil
            end
         end
    end    
end

print()

myInv = getCurrentInventory()
print("Current Robot Inventory:")
for k, v in pairs(myInv) do
    print(k, "Qty:" .. string.format("%u", v.size), "Slot#:" .. v.slot)
end

print("what's needed")
for k, v in pairs(need) do
    print("item: " .. k, "Qty: " .. v)
end

robbie.turnRight()
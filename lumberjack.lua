local rob = require("robot")
local computer = require("computer") -- used for uptime displays
local event = require("event")

-- These comments could be removed to save disk space and memory
-- park the robot right in front of a sapling or hte bottom log of a tree 
-- use an autoclicker to plant a sapling right in front of it on slowest speed (no power requried)
-- place a chest or similar to the right of the robot.
-- the robot will check every 15 seconds if a tree has grown. press any key during this wait to exit the program.
-- when a tree grows, the robot will chop its way straight up 5 blocks, then chop in front of it as it goes down 5 blocks
-- once it chops the last wood, the autoclicker is free to place another sapling.
-- the robot will turn to the right to drop its collected items into the inventory and turn to face the sapling to wait for it to grow again
-- a player doesn't need an axe to chop down a tree, and neither does a robot. it'll happen faster with an axe, but it's not needed

-- it would be possible to have the robot plant the saplings and even apply phytogrow, but that would tak an inventory controller module and more programming

local count = 0

function killTree ()
  for x=1, 4, 1 do
    rob.swingUp()
    rob.up()
  end
  for x=1, 4, 1 do
    rob.swing()
    rob.down()
  end
  rob.swing()
end

function emptyInventory ()
  rob.turnRight()
  for x=1, 16, 1 do
    rob.select(x)
    rob.drop()
  end
  rob.turnLeft()
end
print("Start: robot uptime is " .. computer.uptime())

while true do
  a,testFront = rob.detect()
  if testFront == "solid" then -- saplings arenm't solid, but wood is
    killTree()
    emptyInventory()
    count = count + 1
    print("Cycle " .. count .. " complete. Robot uptime: " .. computer.uptime())  
  end
  check = event.pull(15,"key_down") -- waits 15 seconds for a key to be pressed. if no key is pressed, it will continue the loop.
  if check then -- if a key is pressed, this won't be nil, and the loop is exited
    break 
  end 
end
